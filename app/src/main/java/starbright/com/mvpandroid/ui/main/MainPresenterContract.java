package starbright.com.mvpandroid.ui.main;

import starbright.com.mvpandroid.ui.base.BasePresenterContract;

/**
 * Created by Andreas on 2/11/2018.
 */

public interface MainPresenterContract<V extends MainView> extends BasePresenterContract<V> {
    void getPosts();
}
