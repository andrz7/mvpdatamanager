package starbright.com.mvpandroid.ui.base;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;

import javax.inject.Inject;

import starbright.com.mvpandroid.data.DataManager;
import starbright.com.mvpandroid.data.network.ApiModule;
import starbright.com.mvpandroid.di.component.ApplicationComponent;
import starbright.com.mvpandroid.di.component.DaggerApplicationComponent;
import starbright.com.mvpandroid.di.module.ApplicationModule;

/**
 * Created by Andreas on 1/28/2018.
 */

public class MyApplication extends Application {

    @Inject
    DataManager mDataManager;

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .apiModule(new ApiModule())
                .build();

        mApplicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

    public void setApplicationComponent(ApplicationComponent mApplicationComponent) {
        this.mApplicationComponent = mApplicationComponent;
    }
}
