package starbright.com.mvpandroid.ui.main;

import android.os.Bundle;

import starbright.com.mvpandroid.ui.base.ToolbarActivity;

public class MainActivity extends ToolbarActivity {

    private static final String MAIN_FRAGMENT_TAG = "MAIN_FRAGMENT_TAG ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Testng");
        setBackButtonEnabled(false);
        initFragment();
    }

    private void initFragment() {
        MainFragment fragment = (MainFragment)
                findFragmentByTag(MAIN_FRAGMENT_TAG);
        if (fragment == null) {
            fragment = MainFragment.newInstance();
        }
        setFragment(fragment, MAIN_FRAGMENT_TAG);
    }
}
