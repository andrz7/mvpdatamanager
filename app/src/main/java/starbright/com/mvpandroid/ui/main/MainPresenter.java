package starbright.com.mvpandroid.ui.main;


import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import starbright.com.mvpandroid.data.DataManager;
import starbright.com.mvpandroid.data.network.response.PostResponse;
import starbright.com.mvpandroid.ui.base.BasePresenter;
import starbright.com.mvpandroid.utils.rx.SchedulerProvider;

/**
 * Created by Andreas on 2/11/2018.
 */

public class MainPresenter<V extends MainView> extends BasePresenter<V>
        implements MainPresenterContract<V> {

    @Inject
    public MainPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider,
                         CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void getPosts() {
        getCompositeDisposable().add(
                getDataManager().getPosts()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<PostResponse>>() {
                    @Override
                    public void accept(List<PostResponse> postResponses) throws Exception {
                        getView().onSuccess(postResponses);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                    }
                })
        );
    }
}
