package starbright.com.mvpandroid.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import junit.framework.Assert;

import starbright.com.mvpandroid.R;
import starbright.com.mvpandroid.ui.base.BaseActivity;

/**
 * Created by Andreas on 1/31/2018.
 */

public class ToolbarActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar);

        Toolbar toolbar = findViewById(R.id.top_toolbar);
        setSupportActionBar(toolbar);
        setBackButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    protected void setFragment(Fragment fragment, String tag) {
        if (!((tag == null) ? (getLastBackStackTag() == null) : tag.equals(fragment))) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, fragment, tag)
                    .addToBackStack(tag)
                    .commit();
        }
    }

    protected void setTitle(String title) {
        final ActionBar actionBar = getSupportActionBar();
        Assert.assertNotNull(actionBar);
        actionBar.setTitle(title);
    }

    protected void setTitleVisible(boolean visible) {
        final ActionBar actionBar = getSupportActionBar();
        Assert.assertNotNull(actionBar);
        actionBar.setDisplayShowTitleEnabled(visible);
    }

    protected void setBackButtonEnabled(boolean enabled) {
        final ActionBar actionBar = getSupportActionBar();
        Assert.assertNotNull(actionBar);
        actionBar.setDisplayHomeAsUpEnabled(enabled);
        actionBar.setHomeButtonEnabled(enabled);
    }
}
