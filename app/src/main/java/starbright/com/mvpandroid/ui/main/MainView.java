package starbright.com.mvpandroid.ui.main;

import java.util.List;

import starbright.com.mvpandroid.data.network.response.PostResponse;
import starbright.com.mvpandroid.ui.base.BaseViewContract;

/**
 * Created by Andreas on 2/11/2018.
 */

public interface MainView extends BaseViewContract {
    void showToast(String message);
    void onSuccess(List<PostResponse> data);
}
