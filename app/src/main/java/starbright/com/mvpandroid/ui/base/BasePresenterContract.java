package starbright.com.mvpandroid.ui.base;

import com.androidnetworking.error.ANError;

/**
 * Created by Andreas on 1/31/2018.
 */

public interface BasePresenterContract<V extends BaseViewContract> {
    void onAttach(V mvpView);

    void onDetach();

    void handleApiError(ANError error);

    void setUserAsLoggedOut();
}
