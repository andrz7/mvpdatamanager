package starbright.com.mvpandroid.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import starbright.com.mvpandroid.R;
import starbright.com.mvpandroid.data.network.response.PostResponse;
import starbright.com.mvpandroid.ui.base.BaseFragment;

/**
 * Created by Andreas on 1/31/2018.
 */

public class MainFragment extends BaseFragment implements MainView {

    @Inject
    MainPresenterContract<MainView> presenter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        return view;
    }

    @Override
    protected void setUp(View view) {
        presenter.getPosts();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(List<PostResponse> data) {
        for (PostResponse response : data) {
            System.out.println(response.getmBody());
        }
    }
}
