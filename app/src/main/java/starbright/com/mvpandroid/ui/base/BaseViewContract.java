package starbright.com.mvpandroid.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by Andreas on 1/31/2018.
 */

public interface BaseViewContract {

    void showLoading();

    void hideLoading();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();
}
