package starbright.com.mvpandroid.data.network;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import starbright.com.mvpandroid.data.network.response.PostResponse;

/**
 * Created by Andreas on 1/23/2018.
 */

@Singleton
public class AppApiHelper implements ApiHelper {

    private ApiService mApiService;

    @Inject
    public AppApiHelper(ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public Observable<List<PostResponse>> getPosts() {
        return mApiService.getPosts();
    }
}
