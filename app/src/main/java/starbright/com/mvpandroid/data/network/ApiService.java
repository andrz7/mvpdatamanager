package starbright.com.mvpandroid.data.network;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import starbright.com.mvpandroid.data.network.response.PostResponse;

/**
 * Created by Andreas on 2/11/2018.
 */

public interface ApiService {

    @GET("/posts")
    Observable<List<PostResponse>> getPosts();
}
