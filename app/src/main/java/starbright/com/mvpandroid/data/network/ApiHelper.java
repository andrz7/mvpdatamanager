package starbright.com.mvpandroid.data.network;

import java.util.List;

import io.reactivex.Observable;
import starbright.com.mvpandroid.data.network.response.PostResponse;

/**
 * Created by Andreas on 1/23/2018.
 */

public interface ApiHelper {
    Observable<List<PostResponse>> getPosts();
}
