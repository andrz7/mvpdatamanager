package starbright.com.mvpandroid.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Andreas on 2/11/2018.
 */

public class PostResponse {

    @Expose
    @SerializedName("userId")
    private int mUserId;

    @Expose
    @SerializedName("id")
    private int mId;

    @Expose
    @SerializedName("title")
    private String mTitle;

    @Expose
    @SerializedName("body")
    private String mBody;

    public int getmUserId() {
        return mUserId;
    }

    public int getmId() {
        return mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmBody() {
        return mBody;
    }
}
