package starbright.com.mvpandroid.data;


import starbright.com.mvpandroid.data.db.DbHelper;
import starbright.com.mvpandroid.data.network.ApiHelper;
import starbright.com.mvpandroid.data.prefs.PreferenceHelper;

/**
 * Created by Andreas on 1/23/2018.
 */

public interface DataManager extends DbHelper, PreferenceHelper, ApiHelper{

}
