package starbright.com.mvpandroid.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import starbright.com.mvpandroid.data.DataManager;
import starbright.com.mvpandroid.di.ApplicationContext;
import starbright.com.mvpandroid.di.PreferenceInfo;
import starbright.com.mvpandroid.utils.AppConstants;

/**
 * Created by Andreas on 1/23/2018.
 */

@Singleton
public class AppPreferenceHelper implements PreferenceHelper {

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferenceHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }
}
