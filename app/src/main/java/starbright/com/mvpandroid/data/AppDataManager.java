package starbright.com.mvpandroid.data;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import starbright.com.mvpandroid.data.db.DbHelper;
import starbright.com.mvpandroid.data.network.ApiHelper;
import starbright.com.mvpandroid.data.network.response.PostResponse;
import starbright.com.mvpandroid.data.prefs.PreferenceHelper;
import starbright.com.mvpandroid.di.ApplicationContext;

/**
 * Created by Andreas on 1/23/2018.
 */

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final DbHelper mDbHelper;
    private final PreferenceHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          DbHelper dbHelper,
                          PreferenceHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public Observable<List<PostResponse>> getPosts() {
        return mApiHelper.getPosts();
    }
}
