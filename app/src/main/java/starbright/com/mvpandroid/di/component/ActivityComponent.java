package starbright.com.mvpandroid.di.component;

import dagger.Component;
import starbright.com.mvpandroid.di.PerActivity;
import starbright.com.mvpandroid.di.module.ActivityModule;
import starbright.com.mvpandroid.ui.base.BaseActivity;

/**
 * Created by Andreas on 1/28/2018.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent extends ApplicationComponent {

    void inject(BaseActivity activity);
}
