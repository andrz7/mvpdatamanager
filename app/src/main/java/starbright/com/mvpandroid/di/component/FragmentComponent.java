package starbright.com.mvpandroid.di.component;

import dagger.Component;
import starbright.com.mvpandroid.di.PerFragment;
import starbright.com.mvpandroid.di.module.FragmentPresenterModule;
import starbright.com.mvpandroid.ui.main.MainFragment;
import starbright.com.mvpandroid.ui.base.BaseFragment;

/**
 * Created by Andreas on 1/31/2018.
 */

@PerFragment
@Component(dependencies = ActivityComponent.class, modules = FragmentPresenterModule.class)
public interface FragmentComponent {
    void inject(BaseFragment baseFragment);
    void inject(MainFragment mainFragment);
}
