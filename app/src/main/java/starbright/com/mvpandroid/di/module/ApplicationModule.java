package starbright.com.mvpandroid.di.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import starbright.com.mvpandroid.data.AppDataManager;
import starbright.com.mvpandroid.data.DataManager;
import starbright.com.mvpandroid.data.db.AppDbHelper;
import starbright.com.mvpandroid.data.db.DbHelper;
import starbright.com.mvpandroid.data.network.ApiHelper;
import starbright.com.mvpandroid.data.network.AppApiHelper;
import starbright.com.mvpandroid.data.prefs.AppPreferenceHelper;
import starbright.com.mvpandroid.data.prefs.PreferenceHelper;
import starbright.com.mvpandroid.di.ApplicationContext;
import starbright.com.mvpandroid.di.DatabaseInfo;
import starbright.com.mvpandroid.di.PreferenceInfo;
import starbright.com.mvpandroid.utils.AppConstants;
import starbright.com.mvpandroid.utils.rx.AppSchedulerProvider;
import starbright.com.mvpandroid.utils.rx.SchedulerProvider;

/**
 * Created by Andreas on 1/28/2018.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        this.mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    PreferenceHelper providePreferencesHelper(AppPreferenceHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }
}
