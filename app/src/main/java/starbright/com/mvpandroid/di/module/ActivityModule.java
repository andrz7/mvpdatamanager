package starbright.com.mvpandroid.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import starbright.com.mvpandroid.di.ActivityContext;
import starbright.com.mvpandroid.di.PerActivity;
import starbright.com.mvpandroid.utils.rx.AppSchedulerProvider;
import starbright.com.mvpandroid.utils.rx.SchedulerProvider;

/**
 * Created by Andreas on 1/31/2018.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @PerActivity
    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

}
