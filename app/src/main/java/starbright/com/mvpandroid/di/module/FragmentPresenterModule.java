package starbright.com.mvpandroid.di.module;

import dagger.Module;
import dagger.Provides;
import starbright.com.mvpandroid.di.PerFragment;
import starbright.com.mvpandroid.ui.main.MainPresenter;
import starbright.com.mvpandroid.ui.main.MainPresenterContract;
import starbright.com.mvpandroid.ui.main.MainView;

/**
 * Created by Andreas on 1/31/2018.
 */

@Module
public class FragmentPresenterModule {

    @Provides
    @PerFragment
    MainPresenterContract<MainView> provideMainPresenter(
            MainPresenter<MainView> presenter) {
        return presenter;
    }
}
