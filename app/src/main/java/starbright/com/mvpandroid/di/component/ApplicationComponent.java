package starbright.com.mvpandroid.di.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.disposables.CompositeDisposable;
import starbright.com.mvpandroid.ui.base.MyApplication;
import starbright.com.mvpandroid.data.DataManager;
import starbright.com.mvpandroid.data.network.ApiModule;
import starbright.com.mvpandroid.di.ApplicationContext;
import starbright.com.mvpandroid.di.module.ApplicationModule;
import starbright.com.mvpandroid.utils.rx.SchedulerProvider;

/**
 * Created by Andreas on 1/28/2018.
 */

@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class})
public interface ApplicationComponent {

    void inject(MyApplication myApplication);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();

    SchedulerProvider getSchedulerProvider();

    CompositeDisposable getCompositeDisposable();
}
